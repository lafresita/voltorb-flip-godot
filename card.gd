extends Sprite2D

@onready var value: int = 1
var flipped = false
var marked = false

func flip():
	frame = value
	Events.turn.emit(value)

func _input(event):
	if event is InputEventMouseButton and event.pressed and event.button_index == MOUSE_BUTTON_RIGHT:
		if get_rect().has_point(to_local(event.position)):
			if not flipped:
				if not marked:
					frame = 5
				else:
					frame = 4
				marked = !marked
	if event is InputEventMouseButton and event.pressed and event.button_index == MOUSE_BUTTON_LEFT:
		if get_rect().has_point(to_local(event.position)):
			if not flipped:
				flipped = true
				flip()
