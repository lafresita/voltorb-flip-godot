extends Node2D

@onready var card = preload("res://card.tscn")

var templates = [
	[
		[3,1,6],
		[0,3,6],
		[5,0,6],
		[2,2,6],
		[4,1,6],
	],
	[
		[1,3,7],
		[6,0,7],
		[3,2,7],
		[0,4,7],
		[5,1,7],
	],
	[
		[2,3,8],
		[7,0,8],
		[4,2,8],
		[1,4,8],
		[6,1,8],
	],
	[
		[3,3,8],
		[0,5,8],
		[8,0,10],
		[5,2,10],
		[2,4,10],
	],
	[
		[7,1,10],
		[4,3,10],
		[1,5,10],
		[9,0,10],
		[6,2,10],
	],
	[
		[3,4,10],
		[0,6,10],
		[8,1,10],
		[5,3,10],
		[2,5,10],
	],
	[
		[7,2,10],
		[4,4,10],
		[1,6,10],
		[9,1,10],
		[6,3,10],
	],
	[
		[0,7,10],
		[8,2,10],
		[5,4,10],
		[2,6,10],
		[7,3,10],
	]
]

var gameover = false
var win = false
var twoOpen = 0
var threeOpen = 0

var totalPoints = 0
var roundPoints = 0

var level = 1
var layout
var board = []

var labels = []

func _ready():
	create_cards()
	
	game()

func game():
	while true:
		setup_game(level)
		
		while not gameover and not win:
			handle(await Events.turn)
		
		await get_tree().create_timer(1).timeout
		if win:
			level += 1
			win = false
		else:
			level = 1
			gameover = false
		

func setup_game(lvl):
	$Level.text = "Level " + str(level)
	if lvl == 1:
		totalPoints = 0
	else:
		totalPoints += roundPoints
	$tPoints.text = "Total points:\n" + str(totalPoints)
	roundPoints = 0
	$rPoints.text = "Round points:\n" + str(roundPoints)
	twoOpen = 0
	threeOpen = 0
	clear_cards()
	select_layout()
	
	generate(0)
	generate(2)
	generate(3)
	
	calculate()

func create_cards():
	for i in range(0,5):
		var subarray = []
		for j in range(0,5):
			var c = card.instantiate()
			c.position.x = 60 + 32 * j
			c.position.y = 60 + 32 * i
			add_child(c)
			subarray.append(c)
		board.append(subarray)

func clear_cards():
	for l in labels:
		labels.erase(l)
		l.queue_free()
	for l in labels:
		labels.erase(l)
		l.queue_free()
	for l in labels:
		labels.erase(l)
		l.queue_free()
	for l in labels:
		labels.erase(l)
		l.queue_free()
	for i in range(0,5):
		for j in range(0,5):
			board[i][j].frame = 4
			board[i][j].value = 1
			board[i][j].flipped = false
			board[i][j].marked = false

func select_layout():
	var selected = templates[level-1].pick_random()
	layout = [selected[2], 0, selected[0], selected[1]]

func generate(layer):
	var rng = RandomNumberGenerator.new()
	for v in layout[layer]:
		var flag = false
		while not flag:
			var x = rng.randi_range(0,4)
			var y = rng.randi_range(0,4)
			
			if board[x][y].value == 1:
				board[x][y].value = layer
				flag = true

func calculate():
	# Horizontal
	for i in range(0,5):
		var p = 0
		var v = 0
		for j in range(0,5):
			var val = board[i][j].value
			p += val
			if val == 0:
				v += 1
		var l = Label.new()
		l.text = "P: " + str(p) + "\nV: " + str(v)
		l.position.x = 80 + 32 * 4
		l.position.y = 45 + 32 * i
		l.label_settings = LabelSettings.new()
		l.label_settings.font_size = 10
		l.label_settings.line_spacing = 0
		labels.append(l)
		add_child(l)
	
	# Vertical
	for j in range(0,5):
		var p = 0
		var v = 0
		for i in range(0,5):
			var val = board[i][j].value
			p += val
			if val == 0:
				v += 1
		var l = Label.new()
		l.text = "P: " + str(p) + "\nV: " + str(v)
		l.position.y = 80 + 32 * 4
		l.position.x = 45 + 32 * j
		l.label_settings = LabelSettings.new()
		l.label_settings.font_size = 10
		l.label_settings.line_spacing = 0
		labels.append(l)
		add_child(l)

func handle(val):
	if roundPoints == 0:
		roundPoints += val
	else:
		roundPoints *= val
	$rPoints.text = "Round points:\n" + str(roundPoints)
	if val == 2:
		twoOpen += 1
	elif val == 3:
		threeOpen += 1
	elif val == 0:
		gameover = true
		print("LOST")
	
	if twoOpen == layout[2] and threeOpen == layout[3]:
		win = true
		print("WON")
